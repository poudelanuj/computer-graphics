#include "Object.h"
#include "MatVec.h"
#include "Graphics.h"
#include "Transformation.h"

Vec3 camera(-5,0,-40);
Vec3 fcamera(-5,0,-80);

//Vec3 camera(-25,-25,-25);
Vec3 LookTo(0,0,0);
vector<Vec3> Lpos;


int processInput(){
     Uint8* keystate = SDL_GetKeyState(NULL);

    //continuous-response keys
    if(keystate[SDLK_a]){
        camera.x -= 1;
    }

    if(keystate[SDLK_s]){
        camera.y -= 1;
    }
    if(keystate[SDLK_d]){
        camera.x += 1;
    }
    if(keystate[SDLK_w]){
        camera.y += 1;
    }
    if(keystate[SDLK_z]){
        camera.z += 1;
    }
    if(keystate[SDLK_x]){
        camera.z -= 1;
    }

    if(keystate[SDLK_LEFT]){
        RotateY(camera,-2);
    }

    if(keystate[SDLK_RIGHT]){
        RotateY(camera,2);
    }

    if(keystate[SDLK_UP]){
        RotateX(camera,2);
    }

    if(keystate[SDLK_DOWN]){
        RotateX(camera,-2);
    }


    SDL_Event event;
    while(SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_QUIT:
                return -1;
            case SDL_KEYDOWN:
                if(event.key.keysym.sym == SDLK_ESCAPE)
                    return -1;
            break;
        }
    }
    return 0;
}

int main(int argc, char* args[]){
    Screen S(640,480);

    Object3d obj1(0,0);
    obj1.LoadObject("objects/new.obj");

    Lpos.push_back(Vec3(100,0,0));
    //Lpos.push_back(Vec3(-10,0,0));
    //Lpos.push_back(Vec3(0,100,0));
    //Lpos.push_back(Vec3(0,-100,0));
    //Lpos.push_back(Vec3(0,0,100));
    Lpos.push_back(Vec3(0,0,100));

    while (processInput() != -1){

        S.clrscr();
        S.resetZ();
        //obj1.drawWire(&S,camera,LookTo,1,1);
        obj1.render(&S,camera,LookTo,Lpos,0.5,0.5);
        S.refresh();

    }

    return 0;
}
